"""ryo-iso command line interface."""
import sys

from doit.cmd_base import ModuleTaskLoader
from doit.doit_cmd import DoitMain

import ryo_iso

cfgs = ryo_iso.Config.data.values()
# Only allow init task if config files do not exist
# FIXME: C417 Unnecessary use of map - use a generator expression instead.
if not all(map(lambda x: x.exists(), cfgs)):  # noqa
    import ryo_iso.tasks.init

    module = ryo_iso.tasks.init
else:
    # Initialize configuration context
    ryo_iso.config._ctx = {}
    ryo_iso.config._ctx["config"] = ryo_iso.config.Config()

    import ryo_iso.tasks.main

    module = ryo_iso.tasks.main


def cli(argv=None):
    """Command line interface."""
    if argv is not None:
        DoitMain(ModuleTaskLoader(module)).run(argv)
    else:
        sys.exit(DoitMain(ModuleTaskLoader(module)).run(sys.argv[1:]))
